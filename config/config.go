package config

import (
	"os"
	"strings"
)

type AppConf struct {
	Server     Server
	Logger     Logger
	ClientGRPC ClientGRPC
}

type Logger struct {
	LogLevel        string
	StackTraceLevel string
}

type Server struct {
	Host string
	Port string
}

type ClientGRPC struct {
	Host []string
}

func NewAppConf() *AppConf {
	srv := Server{
		Host: os.Getenv("HOST"),
		Port: os.Getenv("SERVER_PORT"),
	}

	log := Logger{
		LogLevel:        os.Getenv("LOG_LEVEL"),
		StackTraceLevel: os.Getenv("STACK_TRACE_LEVEL"),
	}

	urlBalance := strings.Split(os.Getenv("URL_BALANCE"), ",")
	clientGRPC := ClientGRPC{
		Host: urlBalance,
	}

	return &AppConf{
		Server:     srv,
		Logger:     log,
		ClientGRPC: clientGRPC,
	}
}
