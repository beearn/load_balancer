package balancer

import (
	"context"
	"fmt"
	"gitlab.com/beearn/load_balancer/grpc"
	"go.uber.org/zap"
)

type BinanceLoadBalancer struct {
	client *RoundRobin
	log    *zap.Logger
	grpc.UnimplementedBinanceServer
}

func NewGPRCLoadBalancer(binancer *RoundRobin, log *zap.Logger) *BinanceLoadBalancer {
	return &BinanceLoadBalancer{
		client: binancer,
		log:    log,
	}
}

func (b *BinanceLoadBalancer) GetSymbols(ctx context.Context, in *grpc.MarketIn) (*grpc.SymbolsOut, error) {

	out, err := b.client.Next().GetSymbols(ctx, in)
	if err != nil {
		b.log.Error("error get symbols", zap.Error(err))
		return nil, err
	}

	return out, nil
}

func (b *BinanceLoadBalancer) Klines(ctx context.Context, in *grpc.KlinesIn) (*grpc.KlinesOut, error) {

	out, err := b.client.Next().Klines(ctx, in)
	if err != nil {
		b.log.Error("error get klines", zap.Error(err))
		return nil, err
	}

	return out, nil
}

func (b *BinanceLoadBalancer) Ticker(ctx context.Context, in *grpc.MarketIn) (*grpc.TickerOut, error) {

	out, err := b.client.Next().Ticker(ctx, in)
	if err != nil {
		b.log.Error("error get ticker", zap.Error(err))
		return nil, err
	}

	return out, nil
}

func (b *BinanceLoadBalancer) GetMarketStat(ctx context.Context, in *grpc.MarketIn) (*grpc.PriceChangeStatOut, error) {

	out, err := b.client.Next().GetMarketStat(ctx, in)
	if err != nil {
		b.log.Error("error get market stat", zap.Error(err))
		return nil, err
	}

	return out, nil
}

func (b *BinanceLoadBalancer) GetOrder(ctx context.Context, in *grpc.OrderIn) (*grpc.OrderOut, error) {

	out, err := b.client.Next().GetOrder(ctx, in)
	if err != nil {
		b.log.Error("error get order", zap.Error(err))
		return nil, err
	}

	return out, nil
}

func (b *BinanceLoadBalancer) mustEmbedUnimplementedBinanceServer() {
	fmt.Println("Hello world!")
}
