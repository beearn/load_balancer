package balancer

import (
	"errors"
	"gitlab.com/beearn/load_balancer/grpc"
	"sync"
	"sync/atomic"
)

// ErrorNoObjectsProvided is the error that occurs when no objects are provided.
var ErrorNoObjectsProvided = errors.New("no objects provided")

type RoundRobin struct {
	binanceClients []grpc.BinanceClient
	mux            *sync.Mutex
	next           uint32
}

func NewConnPool(client []grpc.BinanceClient) (*RoundRobin, error) {

	rr, err := New(
		client,
	)

	return rr, err

}

// New returns RoundRobin implementation with roundrobin.
func New(URL []grpc.BinanceClient) (*RoundRobin, error) {
	if len(URL) == 0 {
		return nil, ErrorNoObjectsProvided
	}

	return &RoundRobin{
		mux:            &sync.Mutex{},
		binanceClients: URL,
	}, nil
}

// Next returns the next object.
func (r *RoundRobin) Next() grpc.BinanceClient {
	r.mux.Lock()
	defer r.mux.Unlock()
	n := atomic.AddUint32(&r.next, 1)

	if int(n) > len(r.binanceClients) {
		atomic.StoreUint32(&r.next, 0)
		n = 1
	}

	return r.binanceClients[(int(n)-1)%len(r.binanceClients)]
}
