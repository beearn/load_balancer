package run

import (
	"context"
	"fmt"
	"gitlab.com/beearn/load_balancer/config"
	grpc2 "gitlab.com/beearn/load_balancer/grpc"
	"gitlab.com/beearn/load_balancer/internal/balancer"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net"
	"os"
)

type App struct {
	srv  *grpc.Server
	log  *zap.Logger
	Sig  chan os.Signal
	conf *config.AppConf
}

func NewApp(log *zap.Logger) *App {
	return &App{
		log: log,
	}
}

func (a *App) Bootstrap(conf *config.AppConf) {
	// инициализация grpc клиента
	a.conf = conf

	connPool, err := CreateGRPCPool(a.conf.ClientGRPC.Host)
	if err != nil {
		a.log.Fatal("error init grpc conn", zap.Error(err))
	}

	authGRPC := balancer.NewGPRCLoadBalancer(connPool, a.log)

	// инициализация сервера

	gRPCServer := grpc.NewServer()
	a.srv = gRPCServer

	grpc2.RegisterBinanceServer(a.srv, authGRPC)

}

func (a *App) Run() int {
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.log.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	errGroup.Go(func() error {
		err := a.RunSrv(ctx)
		if err != nil {
			a.log.Error("app: grpc server error", zap.Error(err))
			return err
		}
		return nil
	})
	if err := errGroup.Wait(); err != nil {
		return GeneralError
	}

	return NoError
}

const (
	NoError = iota
	InternalError
	GeneralError
)

func (a *App) RunSrv(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", a.conf.Server.Port))
		if err != nil {
			a.log.Error("grpc server register error", zap.Error(err))
			chErr <- err
			return
		}

		a.log.Info("grpc server started", zap.String("port", a.conf.Server.Port))
		err = a.srv.Serve(l)
		if err != nil {
			a.log.Error("grpc server starting error", zap.Error(err))
			chErr <- err
			return
		}
	}()

	select {
	case <-chErr:
		//s.logger.Error("grpc: server error", zap.Error(err))
		return err
	case <-ctx.Done():
		a.log.Error("grpc: stopping server")
	}
	return nil
}
