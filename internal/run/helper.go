package run

import (
	grpc2 "gitlab.com/beearn/load_balancer/grpc"
	"gitlab.com/beearn/load_balancer/internal/balancer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func CreateGRPCPool(hosts []string) (*balancer.RoundRobin, error) {
	addrClinets := make([]grpc2.BinanceClient, 0, len(hosts))
	for _, domen := range hosts {
		addr, err := gRPCClient(domen)
		if err != nil {
			return nil, err
		}
		addrClinets = append(addrClinets, addr)

	}
	connPool, err := balancer.NewConnPool(addrClinets)
	if err != nil {
		return nil, err
	}
	return connPool, nil
}

func gRPCClient(domain string) (grpc2.BinanceClient, error) {
	conn, err := grpc.Dial(domain,
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	gRPClient := grpc2.NewBinanceClient(conn)

	return gRPClient, nil
}
