module gitlab.com/beearn/load_balancer

go 1.19

require (
	github.com/joho/godotenv v1.5.1
	go.uber.org/zap v1.25.0
	golang.org/x/sync v0.3.0
	google.golang.org/grpc v1.58.1
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230911183012-2d3300fd4832 // indirect
)
