package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/beearn/load_balancer/config"
	"gitlab.com/beearn/load_balancer/internal/logs"
	"gitlab.com/beearn/load_balancer/internal/run"
	"log"
	"os"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	conf := config.NewAppConf()
	logger := logs.NewLogger(conf, os.Stdout)
	app := run.NewApp(logger)

	app.Bootstrap(conf)
	exitCode := app.Run()

	os.Exit(exitCode)

}
